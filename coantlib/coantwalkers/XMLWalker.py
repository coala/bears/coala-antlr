"""XMLWalker module."""

from coantlib.ASTWalker import ASTWalker
from coantlib.coantparsers.XML import XMLParserVisitor
from coantlib.coantparsers.XML import XMLParser
from coantlib.commonutils import get_line_and_col
from coantlib.NodeData import NodeData

import types
import antlr4


class XMLWalker(ASTWalker):
    """Walker class for XML's ANTLR Parse Tree."""

    def __init__(self, filename, file, lang):
        """Initialise the parent classes for creating a Parser."""
        super(XMLWalker, self).__init__(file, filename, lang)

    def get_element_nodes_location(self):
        """
        Retrieve all XML with their start and end tag location.

        :returns: list of NodeData elements
                  The other field in NodeData contains locations of
                  start and end tags of that xml node along with locations
                  of attributes of that node.

        :Example:

        The return value is a list of the form::

            [
                'NodeData(element)': NodeData.other = {
                        '<' : [NodeData(), NodeData(), ... ],
                        '>' : [NodeData(), NodeData(), ... ],
                        'attributes': [NodeData(), NodeData(), ... ],
                 },
                 ...
            ]
        """
        visitor = XMLParserVisitor()
        visitor.results = []
        visitor.depth = 0

        def element_walk(self, ctx):
            loc_data = {
                '<': [],
                '>': [],
                '/': [],
                '/>': [],
                'attributes': [],
                'depth': self.depth,
            }
            self.depth += 1
            for i in range(0, ctx.getChildCount()):
                child = ctx.getChild(i)
                if type(child) == antlr4.tree.Tree.TerminalNodeImpl:
                    if child.getText() in ['<', '>', '/', '/>']:
                        lin, col = get_line_and_col(child)
                        loc_data[child.getText()].append(
                            NodeData(child.getText(),
                                     lin,
                                     col
                                     )
                            )
                elif type(child) == XMLParser.AttributeContext:
                    lin, col = get_line_and_col(child)
                    node = NodeData(child.getText(), lin, col)
                    loc_data['attributes'].append(node)
            lin, col = get_line_and_col(ctx)
            self.results.append(NodeData(str(ctx.Name(0)),
                                         lin,
                                         col,
                                         other=loc_data))
            res = self.visitChildren(ctx)
            self.depth -= 1
            return res
        visitor.visitElement = types.MethodType(element_walk, visitor)
        visitor.visit(self.treeRoot)

        return visitor.results
