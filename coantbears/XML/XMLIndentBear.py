"""XMLIndentBear module."""

from coalib.results.Diff import Diff
from coalib.results.Result import Result
from coantlib.ASTBear import ASTBear


def indent_line(line_no, indent_level, file):
    """Return a diff with line_no as correctly indented line."""
    file_diff = Diff(file)
    correct_line = ' ' * indent_level + file[line_no-1].lstrip()
    file_diff.modify_line(line_no, correct_line)
    return file_diff


class XMLIndentBear(ASTBear):
    """Check for indentation in XML files."""

    LANGUAGES = {'XML'}
    CAN_FIX = {'Formatting'}

    def run(self,
            filename,
            file,
            style: int = 1,
            default_indent: int = 4,
            ):
        """
        Beautify indents in XML files.

        Supports two indent styles,

        Style 1::

            <tag attr1=val1
                 attr2=val2>
                 Data
                 <tag2 attr1=val1 />
            </tag>

        Style 2::

            <tag
              attr1=val1
              attr2=val2
            >
                <tag2
                  attr1=val1
                />
              Data
            </tag>
        """
        super().run(filename, file)
        locations = self.walker.get_element_nodes_location()
        for loc in locations:
            # one liner
            if (len(loc.other['<']) == 2 and
                    loc.other['<'][0].lineNo == loc.other['<'][1].lineNo):
                depth = loc.other['depth']
                topen = loc

                if topen.colNo != depth * default_indent + 1:
                    yield Result.from_values(
                        origin=self,
                        message=('{} not indented '
                                 'properly !'.format(loc.text)
                                 ),
                        file=filename,
                        line=topen.lineNo,
                        diffs={
                            filename: indent_line(
                                topen.lineNo,
                                depth * default_indent,
                                file
                                )
                            },
                        )
            else:
                for tag_open in loc.other['<']:
                    # check the opening and closing tag's indent depth
                    tag_col = tag_open.colNo
                    depth = loc.other['depth']

                    if tag_col != depth * default_indent + 1:
                        yield Result.from_values(
                            origin=self,
                            message=('{} not indented '
                                     'properly !'.format(loc.text)
                                     ),
                            file=filename,
                            line=tag_open.lineNo,
                            column=tag_open.colNo,
                            end_line=tag_open.lineNo,
                            end_column=tag_open.colNo+1,
                            diffs={
                                filename: indent_line(
                                    tag_open.lineNo,
                                    depth * default_indent,
                                    file
                                    )
                                },
                            )
                # check for the closing of this tag if it has no data
                if len(loc.other['<']) == 1:
                    close_tag = loc.other['/>'][0]
                    depth = loc.other['depth'] * default_indent
                    # this tag has no data
                    if style == 1:
                        if len(loc.other['attributes']) != 0:
                            last_attr_line = loc.other['attributes'][-1].lineNo
                        else:
                            last_attr_line = loc.lineNo

                        if close_tag.lineNo != last_attr_line:
                            correct_line = file[last_attr_line-1][:-1] + '/>\n'
                            df = Diff(file)
                            df.delete_line(close_tag.lineNo)
                            df.modify_line(last_attr_line, correct_line)
                            yield Result.from_values(
                                origin=self,
                                message=('{} not indented as per '
                                         'set style !'.format(loc.text)
                                         ),
                                file=filename,
                                line=close_tag.lineNo,
                                column=close_tag.colNo,
                                end_line=close_tag.lineNo,
                                end_column=close_tag.colNo+2,
                                diffs={filename: df},
                                )
                    elif style == 2:
                        correct_line = None
                        if len(loc.other['attributes']) != 0:
                            last_attr_line = loc.other['attributes'][-1].lineNo
                        else:
                            last_attr_line = loc.lineNo
                        if last_attr_line + 1 != close_tag.lineNo:
                            l, c = close_tag.lineNo, close_tag.colNo
                            correct_line = (file[l-1][:c-1].rstrip() + '\n' +
                                            ' ' * depth + '/>')
                        elif close_tag.colNo != depth+1:
                            correct_line = (' ' * depth +
                                            file[close_tag.lineNo-1].lstrip())
                        if correct_line:
                            df = Diff(file)
                            df.modify_line(last_attr_line, correct_line)
                            yield Result.from_values(
                                origin=self,
                                message=('{} not indented as per '
                                         'set style !'.format(loc.text)
                                         ),
                                file=filename,
                                line=close_tag.lineNo,
                                column=close_tag.colNo,
                                end_line=close_tag.lineNo,
                                end_column=close_tag.colNo+2,
                                diffs={filename: df},
                                )
                    else:
                        raise AssertionError('Invalid style {}.'.format(style))
                else:
                    start_tg = loc.other['>'][0]
                    end_tg = loc.other['>'][1]
                    diff = Diff(file)
                    if style == 1:
                        if len(loc.other['attributes']) != 0:
                            last_attr_line = loc.other['attributes'][-1].lineNo
                        else:
                            last_attr_line = loc.lineNo
                        correct_line = None

                        if end_tg.lineNo != loc.other['<'][1].lineNo:
                            orig_line = file[loc.other['>'][1].lineNo-1][:-1]
                            correct_line = orig_line + '>\n'
                            diff.modify_line(
                                loc.other['<'][1].lineNo, correct_line)
                            diff.delete_line(end_tg.lineNo)

                        if start_tg.lineNo != last_attr_line:
                            orig_line = file[last_attr_line-1][:-1]
                            correct_line = orig_line + '>\n'
                            diff.delete_line(start_tg.lineNo)
                            diff.modify_line(last_attr_line, correct_line)

                        if correct_line:
                            yield Result.from_values(
                                origin=self,
                                message=('{} not indented as per '
                                         'set style !'.format(loc.text)
                                         ),
                                file=filename,
                                diffs={filename: diff},
                                )

                att_num = 0 if style == 1 else 1
                attr_dep = (loc.other['depth'] + 1) * default_indent
                for attr in loc.other['attributes']:
                    correct_line = None
                    affected_line = None
                    df = Diff(file)
                    if att_num == 0 and attr.lineNo != loc.lineNo:
                        orig_line = file[loc.lineNo-1][:-1]
                        correct_line = (orig_line + ' ' +
                                        file[attr.lineNo-1].lstrip())
                        df.delete_line(attr.lineNo)
                        affected_line = loc.lineNo
                    elif (attr.lineNo != loc.lineNo + att_num and
                          file[attr.lineNo-1][:attr.colNo-1].lstrip() != ''):
                        orig_line = file[attr.lineNo-1][:-1]
                        correct_line = (orig_line[:attr.colNo-1] + '\n' +
                                        ' ' * attr_dep +
                                        orig_line[attr.colNo-1:])
                        affected_line = attr.lineNo
                    elif attr.colNo != attr_dep + 1 and att_num != 0:
                        orig_line = file[attr.lineNo-1].lstrip()
                        correct_line = ' ' * attr_dep + orig_line
                        affected_line = attr.lineNo

                    if correct_line:
                        df.modify_line(affected_line, correct_line)
                        yield Result.from_values(
                            origin=self,
                            message=('Attribute not indented as per '
                                     'set style !'.format(loc.text)
                                     ),
                            file=filename,
                            line=attr.lineNo,
                            column=attr.colNo,
                            end_line=attr.lineNo,
                            end_column=attr.colNo+1+len(str(attr)),
                            diffs={filename: df},
                            )

                    att_num += 1
